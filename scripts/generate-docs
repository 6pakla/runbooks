#!/usr/bin/env ruby
# frozen_string_literal: true
require 'yaml'
require 'erb'
require 'ostruct'
require 'open3'
require 'pathname'

START_MARKER = "<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->"
STOP_MARKER = "<!-- END_MARKER -->"

def find_troubleshooting_docs(service_name)
  docs = []
  troubleshooting_docs_glob = File.join(__dir__, "..", "docs", "*", "*.md")
  dest_path = Pathname.new(File.join(__dir__, "..", "docs", service_name))
  Open3.popen3("grep -li '\\b#{service_name}\\b' #{troubleshooting_docs_glob}") do |stdin, stdout, stderr, wait_thr|
    while line = stdout.gets
      file_name = Pathname.new(line.chomp)
      relative = file_name.relative_path_from(dest_path)
      docs.push(relative) unless line.include? "service-"
    end
  end

  docs
end

def replace_header(file, header)
  lines = File.readlines(file)
  start = lines.find_index { |line| line.chomp == START_MARKER }

  throw "Unable to find start marker for #{file}" if start.nil?
  stop = lines.find_index { |line| line.chomp == STOP_MARKER }
  throw "Unable to find stop marker for #{file}" if stop.nil?

  lines.slice! start, stop - start + 1
  lines.insert start, header

  IO.write(file, lines.join)
end

def generate_header(service, teams, tier)
  service_name = service["name"]
  troubeshooting_docs = find_troubleshooting_docs(service_name)
  template = <<-MARKDOWN_EOF
#{START_MARKER}
#  <%= service["name"].capitalize %> Service

* **Responsible Teams**:
<%- teams.each do |team| -%>
  * [<%= team["name"] %>](<%= team["url"] %>). **Slack Channel**: [\#<%= team["slack_channel"] %>](https://gitlab.slack.com/archives/<%= team["slack_channel"] %>)
<%- end -%>
* **General Triage Dashboard**: https://dashboards.gitlab.net/d/26q8nTzZz/service-platform-metrics?from=now-6h&to=now&var-prometheus_ds=Global&var-environment=gprd&var-type=<%= service_name %>&orgId=1
* **Alerts**: https://alerts.gprd.gitlab.net/#/alerts?filter=%7Btype%3D%22<%= service_name %>%22%2C%20tier%3D%22<%= service["tier"] %>%22%7D
<% if service["label"] -%>
* **Label**: gitlab-com/gl-infra/production~"<%= service["label"] %>"
<% end -%>
<% if service["sentry_slug"] -%>
* **Sentry**: https://sentry.gitlab.net/<%= service["sentry_slug"] %>
<% end -%>
<%- if service["grafana_folder"] -%>
* **Grafana Folder**: https://dashboards.gitlab.net/dashboards/f/<%= service["grafana_folder"] %>
<% end -%>

## Logging

<%- service["technical"]["logging"].each do |logging_location| -%>
* [<%= logging_location["name"] %>](<%= logging_location["permalink"] %>)
<%- end -%>

<% if troubeshooting_docs && !troubeshooting_docs.empty? -%>
## Troubleshooting Pointers

<%- troubeshooting_docs.each do |troubeshooting_doc| -%>
* [<%= troubeshooting_doc %>](<%= troubeshooting_doc %>)
<%- end -%>
<% end -%>
#{STOP_MARKER}
  MARKDOWN_EOF

  data = OpenStruct.new(
    teams: teams,
    service: service,
    tier: tier,
    troubeshooting_docs: troubeshooting_docs
  )

  ERB.new(template, trim_mode: "-").result(data.instance_eval { binding })
end

def generate_docs
  service_mapping_yaml = YAML.load_file(File.join(__dir__, "..", "services", "service-catalog.yml"))
  services = service_mapping_yaml["services"]

  team_map = service_mapping_yaml["teams"].each_with_object({}) { |team, map| map[team["name"]] = team;  }
  tier_map = service_mapping_yaml["tiers"].each_with_object({}) { |tier, map| map[tier["name"]] = tier;  }
  service_map = service_mapping_yaml["services"].each_with_object({}) { |service, map| map[service["name"]] = service; }

  services.each do |service|
    service_name = service["name"]
    teams = service["teams"].map { |team| team_map[team] }
    tier = tier_map[service["tier"]]

    troubleshooting_doc = File.join(__dir__, "..", "docs", service_name.to_s, "service-#{service_name}.md")
    details = generate_header(service, teams, tier)
    if File.exist?(troubleshooting_doc)
      replace_header(troubleshooting_doc, details)
    else
      File.write(troubleshooting_doc, details)
    end
  end

  Dir.glob(File.join(__dir__, "..", "docs", "*", "service-*.md")).each do |file|
    referred_service_name = File.basename(file, ".md").sub("service-", "")
    File.delete(file) unless service_map[referred_service_name]
  end
end

begin
  generate_docs
  # rescue StandardError => e
  #   STDERR.puts "error: #{e.message}"
  #   exit 1
end
